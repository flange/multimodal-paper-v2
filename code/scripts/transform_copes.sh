#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Run warping of copes to template space
# Pass subject ID, task, cope number, smoothing and registration type as input parameters
source ~/.bash_profile

subject_id=$1
task_id=$2
cope_id=$3
smooth_id=$4
reg_id=$5
cope_file="/well/win-hcp/HCP-YA/users/sgk882/multimodal_validation/data/${subject_id}/T1w/Results/${task_id}/${task_id}_hp200_s${smooth_id}_level2.feat/StandardVolumeStats/cope${cope_id}.feat/cope1"
out_dir="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/${subject_id}/OMM/Results/${task_id}_hp200_s${smooth_id}_level2"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

out_file="${out_dir}/cope${cope_id}_${reg_id}"
warp_file="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/${subject_id}/warp_${reg_id}"
ref_file="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/template/t1_2mm"
mat_file="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/${subject_id}/t1_to_template_affine.mat"
if [[ ${reg_id} == "af" ]]
then
  applywarp \
    -i ${cope_file} \
    -r ${ref_file} \
    -o ${out_file} \
    --premat=${mat_file} \
    --interp=trilinear \
    -v
elif [[ ${reg_id} == "fnirt" || ${reg_id} == "fnirt_hcp" ]]
then
  applywarp \
    -i ${cope_file} \
    -r ${ref_file} \
    -o ${out_file} \
    -w ${warp_file} \
    --interp=trilinear \
    -v
elif [[ ${reg_id} == "ants_1mil" || ${reg_id} == "drtamas_mm_1mil" || ${reg_id} == "drtamas_mm_2mil" ]]
then
  applywarp \
    -i ${cope_file} \
    -r ${ref_file} \
    -o ${out_file} \
    -w "${warp_file}_combined" \
    --interp=trilinear \
    -v
else
  applywarp \
    -i ${cope_file} \
    -r ${ref_file} \
    -o ${out_file} \
    -w ${warp_file} \
    --premat=${mat_file} \
    --interp=trilinear \
    -v
fi