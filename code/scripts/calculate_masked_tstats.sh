
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
source ~/.bash_profile


# Calculate tstats in masked regions
project_dir="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/group/OMM/Results"
task_id=$1
max_cope_id=$2
smooth_id=$3
reg_a_id=$4
reg_b_id=$5
out_dir="${project_dir}/${task_id}_hp200_s${smooth_id}/${reg_a_id}_vs_${reg_b_id}"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

out_file="${out_dir}/masked_tstats.txt"
echo "cope ${reg_a_id} ${reg_b_id} ${reg_a_id}-${reg_b_id}" > ${out_file}
for cope_id in $(seq ${max_cope_id})
do
  echo ">>> Running operations for cope ${cope_id}" 
  mask_file="${out_dir}/cope${cope_id}_mask_intersect"
  tstat_a_file="${project_dir}/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_a_id}/mean_activation_tstat1"
  tstat_b_file="${project_dir}/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_b_id}/mean_activation_tstat1"
  tstat_a_masked_file="${out_dir}/cope${cope_id}_tstat1_${reg_a_id}_masked"
  tstat_b_masked_file="${out_dir}/cope${cope_id}_tstat1_${reg_b_id}_masked"
  tstat_diff_masked_file="${out_dir}/cope${cope_id}_tstat1_${reg_a_id}_sub_${reg_b_id}_masked"

  fslmaths \
    ${tstat_a_file} \
    -mul ${mask_file} \
    ${tstat_a_masked_file}
  fslmaths \
    ${tstat_b_file} \
    -mul ${mask_file} \
    ${tstat_b_masked_file}
  fslmaths \
    ${tstat_a_file} \
    -sub ${tstat_b_file} \
    -mul ${mask_file} \
    ${tstat_diff_masked_file}
  
  mean_tstat_a_masked=$(fslstats ${tstat_a_masked_file} -M)
  mean_tstat_b_masked=$(fslstats ${tstat_b_masked_file} -M)
  mean_tstat_diff_masked=$(fslstats ${tstat_diff_masked_file} -M)
  echo "${cope_id} ${mean_tstat_a_masked} ${mean_tstat_b_masked} ${mean_tstat_diff_masked}" >> ${out_file}
done
echo ">>> Completed"