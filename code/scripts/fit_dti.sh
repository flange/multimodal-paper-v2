#!/bin/bash
# Fit the DTI model to HCP data
# Pass subject ID as input parameter
source ~/.bash_profile

subject_id=$1
hcp_dir="/well/win-hcp/HCP-YA/subjectsAll"
subject_diffusion_dir="${hcp_dir}/${subject_id}/T1w/Diffusion"

# Create output directory
echo ">>> Creating directory ../data/${subject_id}"
if ! [[ -d ../data/${subject_id} ]];
then
  mkdir -p ../data/${subject_id}
else
  echo "Directory already exists"
fi
# Extract only the b=1000 volumes
echo ">>> Extracting only b=1000 and b=0 volumes"
select_dwi_vols \
    ${subject_diffusion_dir}/data \
    ${subject_diffusion_dir}/bvals \
    ../data/${subject_id}/dti_data \
    0 \
    -b 1000 \
    -obv ${subject_diffusion_dir}/bvecs
# Run dtifit on b=1000 volumes
echo ">>> Running DTIFIT with weighted least squares"
dtifit \
  -k ../data/${subject_id}/dti_data \
  -o ../data/${subject_id}/dti \
  -m ${subject_diffusion_dir}/nodif_brain_mask \
  -r ../data/${subject_id}/dti_data.bvec \
  -b ../data/${subject_id}/dti_data.bval \
  --save_tensor \
  -w \
  -V
echo ">>> Removing extracted data"
rm ../data/${subject_id}/dti_data*
echo ">>> Subject ${subject_id} completed"