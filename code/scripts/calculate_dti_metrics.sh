
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate DTI similarity metrics 
source ~/.bash_profile
conda activate neuro

subject_id=$1
reg_id=$2

project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
subject_dir="${project_dir}/data/${subject_id}/dti"
subject_prefix="${subject_dir}/dti_tensor_${reg_id}_decomp"
template_dir="${project_dir}/template/dti"
template_prefix="${template_dir}/dti_decomp"
mask_br="${template_dir}/mask_dti_decomp_whole_brain"
mask_wm="${template_dir}/mask_dti_decomp_FA_thr_0_2"
mask_cl="${template_dir}/dti_decomp_CL"
mask_cp="${template_dir}/dti_decomp_CP"
output_dir="${subject_dir}"
output_csv="${output_dir}/dti_metrics_${reg_id}.csv"

# Create output directory
echo ">>> Creating directory ${output_dir}"
if ! [[ -d ${output_dir} ]];
then
  mkdir -p ${output_dir}
else
  echo "Directory already exists"
fi

# Run metric calculation
echo ">>> Running calculate_dti_metrics.py"
${project_dir}/scripts/calculate_dti_metrics.py \
    -a ${subject_prefix} \
    -b ${template_prefix} \
    -A ${subject_id} \
    -B OMM-0 \
    -mb ${mask_br} \
    -mw ${mask_wm} \
    -ml ${mask_cl} \
    -mp ${mask_cp} \
    -r ${reg_id} \
    -o ${output_csv}

echo ">>> Subject ${subject_id} completed"