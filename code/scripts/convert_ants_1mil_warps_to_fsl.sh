
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Convert warps from ANTs format to FSL format, including combining affine and nonlinear.
# NOTE: We expect these warps to also include the affine component.
source ~/.bash_profile

subject_id=$1
project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
subject_ants_dir="${project_dir}/data/${subject_id}/ants"

# Combine affine & nonlinear
echo ">>> Running ComposeMultiTransform"
ComposeMultiTransform \
  3 \
  ${subject_ants_dir}/warp_ants_1mil_combined_orig.nii.gz \
  -R ${project_dir}/template/t1.nii.gz \
  ${subject_ants_dir}/ants_1mil_1Warp.nii.gz \
  ${subject_ants_dir}/ants_1mil_0GenericAffine.mat

# Delete unncessary inverse warp
rm ${subject_ants_dir}/ants_1mil_1InverseWarp.nii.gz

module purge
module load ConnectomeWorkbench

# Use wb_command to do the conversion
echo ">>> Running wb_command"
wb_command \
  -convert-warpfield \
  -from-itk ${subject_ants_dir}/warp_ants_1mil_combined_orig.nii.gz \
  -to-fnirt ${subject_ants_dir}/../warp_ants_1mil_combined.nii.gz ${project_dir}/template/t1.nii.gz

echo ">>> Subject ${subject_id} completed"