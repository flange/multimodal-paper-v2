
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
source ~/.bash_profile


# Create masks of the intersection of significantly active voxels for pairs of registration
# methods.
project_dir="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/group/OMM/Results"
task_id=$1
cope_id=$2
smooth_id=$3
reg_a_id=$4
reg_b_id=$5
pval_a_file="${project_dir}/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_a_id}/mean_activation_vox_corrp_tstat1"
pval_b_file="${project_dir}/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_b_id}/mean_activation_vox_corrp_tstat1"
out_dir="${project_dir}/${task_id}_hp200_s${smooth_id}/${reg_a_id}_vs_${reg_b_id}"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

out_file="${out_dir}/cope${cope_id}_mask_intersect"

echo ">>> Running fslmaths" 
fslmaths \
  ${pval_a_file} \
  -thr 0.95 \
  -bin \
  -mul ${pval_b_file} \
  -thr 0.95 \
  -bin \
  ${out_file}

echo ">>> Completed"