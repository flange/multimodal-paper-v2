#!/bin/bash
# Create masks of the intersection of significantly active voxels for pairs of registration
# methods.

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

task_list=(tfMRI_EMOTION tfMRI_GAMBLING tfMRI_LANGUAGE tfMRI_MOTOR tfMRI_RELATIONAL tfMRI_SOCIAL tfMRI_WM)
cope_list=(6 6 6 26 6 6 30)
#reg_a_list=(t1_x2 mm_x2 mm_x2 mm_x2 t1 mm_nudge t1 mm_nudge mm_nudge t1_x2 t1_x2 mm_x2)
#reg_b_list=(t1_x4 mm_nudge t1_x2 fnirt af t1 t1_x2 mm t1_x2 fnirt_hcp fnirt dti_x2)
reg_a_list=(mm_1mil)
reg_b_list=(mm_x2)

for i in ${!task_list[@]}
do
  task_id=${task_list[i]}
  for smooth_id in 0
  do
    submit_file="../submit/job_create_cope_masks_${task_id}_s${smooth_id}.sh"
    if [[ -f ${submit_file} ]];
    then
      rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    for cope_id in $(seq ${cope_list[i]})
    do
      for j in ${!reg_a_list[@]}
      do
        reg_a_id=${reg_a_list[j]}
        reg_b_id=${reg_b_list[j]}
        echo "./create_cope_masks.sh ${task_id} ${cope_id} ${smooth_id} ${reg_a_id} ${reg_b_id}" >> ${submit_file}
      done
    done
    fsl_sub -t ${submit_file} -l logs/log_create_cope_masks/${task_id}_s${smooth_id}
  done
done