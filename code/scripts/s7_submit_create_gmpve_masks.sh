#!/bin/bash
# Mask grey matter partial volume estimates by cope activation

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

task_list=(tfMRI_EMOTION tfMRI_GAMBLING tfMRI_LANGUAGE tfMRI_MOTOR tfMRI_RELATIONAL tfMRI_SOCIAL tfMRI_WM)
cope_list=(6 6 6 26 6 6 30)
reg_list=(mm_1mil)
#reg_list=(t1_x4 af mm_x2 mm_nudge mm_4mil mm t1 t1_x2 fnirt fnirt_hcp mm_x2_centre fnirt dti_x2 drtamas_mm)

for i in ${!task_list[@]}
do
  task_id=${task_list[i]}
  for smooth_id in 0
  do
    submit_file="../submit/job_create_gmpve_masks_${task_id}_s${smooth_id}.sh"
    if [[ -f ${submit_file} ]];
    then
      rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    for cope_id in $(seq ${cope_list[i]})
    do
      for reg_id in ${reg_list[@]}
      do
        echo "./create_gmpve_masks.sh ${task_id} ${cope_id} ${smooth_id} ${reg_id} " >> ${submit_file}
      done
    done
    fsl_sub -t ${submit_file} -l logs/log_create_gmpve_masks/${task_id}_s${smooth_id}
  done
done