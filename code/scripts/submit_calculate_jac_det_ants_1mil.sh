#!/bin/bash
# Parse list of HCP subjects and create array job for converting ANTs warps to FSL

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi
submit_file="../submit/job_calculate_jac_det_ants_1mil.sh"
if [[ -f ${submit_file} ]];
then
  rm ${submit_file}
fi
touch ${submit_file}
chmod u+x $submit_file

# Loop over subjects
subject_id_file="../subjects.txt"
for subject_id in $(cat ${subject_id_file})
do
  # Populate submit script
  echo "./calculate_jac_det_ants_1mil.sh ${subject_id}" >> ${submit_file}
done

# Submit task array job
fsl_sub -t ${submit_file} -l logs/log_calculate_jac_det_ants_1mil