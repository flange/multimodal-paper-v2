#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate 5th and 95th percentile of the Jacobian determinant within the brain mask
source ~/.bash_profile

reg_id=$1
mask="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/template/mask_t1"
out_file="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/group/OMM/Results/jacs/${reg_id}_jac_ranges.txt"
if [[ ${reg_id} == "fnirt" || ${reg_id} == "fnirt_hcp" ]]
then
  jacs=`ls /well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/*/fnirt/jac_${reg_id}.nii.gz`
else
  jacs=`ls /well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/*/jac_${reg_id}.nii.gz`
fi

echo "5th 95th 2nd 98th 1st 99th" > ${out_file}
for jac in ${jacs}
do
  fslstats ${jac} -k ${mask} -p 5 -p 95 -p 2 -p 98 -p 1 -p 99 >> ${out_file}
done