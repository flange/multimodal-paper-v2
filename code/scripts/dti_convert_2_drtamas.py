#!/usr/bin/env python3
#
# Date: 27/07/2022
# Author: Frederik J Lange
# Copyright: FMRIB 2022

import copy
from fsl.data import image
import argparse

from numpy import require

def dti_fsl_to_drtamas(img_in):
    # Reorder the 6 unique DTI components from the FSL convention. Note
    # that apart from reordering, the x-values must also be swapped
    img_out = copy.deepcopy(img_in)
    img_out[:,:,:,0] = 1e6*img_in[:,:,:,0]
    img_out[:,:,:,1] = 1e6*img_in[:,:,:,3]
    img_out[:,:,:,2] = 1e6*img_in[:,:,:,5]
    img_out[:,:,:,3] = -1e6*img_in[:,:,:,1]
    img_out[:,:,:,4] = -1e6*img_in[:,:,:,2]
    img_out[:,:,:,5] = 1e6*img_in[:,:,:,4]
    return img_out

flags = {
    'input'  : ('-i', '--input'),
    'output' : ('-o', '--output')
}
helps = {
    'input'  : 'FSL format DTI image',
    'output' : 'DR-TAMAS format DTI image'
}

parser = argparse.ArgumentParser(description='Convert from FSL format DTI volume to DR-TAMAS format')
parser.add_argument(*flags['input'],
                    help=helps['input'],
                    required=True)
parser.add_argument(*flags['output'],
                    help=helps['output'],
                    required=True)
args = parser.parse_args()

my_img = image.Image(args.input)
my_img = dti_fsl_to_drtamas(my_img)
my_img.save(args.output)