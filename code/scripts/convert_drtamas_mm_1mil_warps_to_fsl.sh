
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Convert warps from DR-TAMAS format to FSL format.
# NOTE: We expect these warps to also include the affine component.
#source ~/.bash_profile
module purge
module add ConnectomeWorkbench

subject_id=$1
project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
subject_dti_dir="${project_dir}/data/${subject_id}/drtamas_mm_1mil"

# Use wb_command to do the conversion
echo ">>> Running wb_command"
wb_command \
  -convert-warpfield \
  -from-itk ${subject_dti_dir}/warp_drtamas_mm_1mil_combined_orig.nii.gz \
  -to-fnirt ${subject_dti_dir}/../warp_drtamas_mm_1mil_combined.nii.gz ${project_dir}/template/t1.nii.gz

echo ">>> Subject ${subject_id} completed"