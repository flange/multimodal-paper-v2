
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Combine affine and nonlinear warps
source ~/.bash_profile

subject_id=$1
reg_id=$2

project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
subject_dir="${project_dir}/data/${subject_id}"
ref_file="${project_dir}/template/t1"
aff_file="${subject_dir}/t1_to_template_affine.mat"
warp_file="${subject_dir}/warp_${reg_id}"
out_file="${subject_dir}/warp_${reg_id}_combined"

# Run convertwarp
echo ">>> Running convertwarp"
convertwarp \
  --ref=${ref_file} \
  --premat=${aff_file} \
  --warp1=${warp_file} \
  --out=${out_file} \
  --verbose
echo ">>> Subject completed"