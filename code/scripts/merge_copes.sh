
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
source ~/.bash_profile

# Merge warped copes
task_id=$1
cope_id=$2
smooth_id=$3
reg_id=$4
in_files=$(ls /well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/*/OMM/Results/${task_id}_hp200_s${smooth_id}_level2/cope${cope_id}_${reg_id}.nii.gz)
out_dir="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/group/OMM/Results/${task_id}_hp200_s${smooth_id}"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

out_file="${out_dir}/cope${cope_id}_${reg_id}_merged"

echo ">>> Running fslmerge" 
fslmerge \
  -t \
  ${out_file} \
  ${in_files}
echo ">>> Completed"