#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Run Multimodal registration using MMORF
# Pass subject ID as input parameter
source ~/.bash_profile

subject_id=$1
project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
hcp_dir="/gpfs3/well/win-hcp/HCP-YA/subjectsAll"
subject_t1_dir="${hcp_dir}/${subject_id}/T1w"
subject_dti_dir="${project_dir}/data/${subject_id}"

# Create output directory
echo ">>> Creating directory ../data/${subject_id}"
if ! [[ -d ../data/${subject_id} ]];
then
  mkdir -p ../data/${subject_id}
else
  echo "Directory already exists"
fi
# MMORF subject to template
echo ">>> Running MMORF between subject and template T1w & DTI images"
SINGULARITY_BIND=${hcp_dir},${project_dir}
export SINGULARITY_BIND
mmorf_path="/gpfs3/well/win-fmrib-analysis/users/sgk882/mmorf.sif"
singularity run --nv ${mmorf_path} \
  --config ${project_dir}/config/config_MM_x2.ini \
  --img_warp_space ${project_dir}/template/t1 \
  --img_ref_scalar ${project_dir}/template/t1 \
  --img_mov_scalar ${subject_t1_dir}/T1w_acpc_dc_restore_brain \
  --aff_ref_scalar ${project_dir}/template/identity.mat \
  --aff_mov_scalar ${project_dir}/data/${subject_id}/t1_to_template_affine.mat \
  --mask_ref_scalar NULL \
  --mask_mov_scalar NULL \
  --img_ref_tensor ${project_dir}/template/dti_nudge_0_1_1 \
  --img_mov_tensor ${subject_dti_dir}/dti_tensor \
  --aff_ref_tensor ${project_dir}/template/identity.mat \
  --aff_mov_tensor ${project_dir}/data/${subject_id}/t1_to_template_affine.mat \
  --mask_ref_tensor ${project_dir}/template/mask_dti \
  --mask_mov_tensor NULL \
  --warp_out ${project_dir}/data/${subject_id}/warp_mm_x2 \
  --jac_det_out ${project_dir}/data/${subject_id}/jac_mm_x2 \
  --bias_out ${project_dir}/data/${subject_id}/bias_mm_x2
echo ">>> Subject ${subject_id} completed"