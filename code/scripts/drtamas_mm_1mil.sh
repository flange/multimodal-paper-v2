#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Run Multimodal registration using DR-TAMAS
# Pass subject ID as input parameter
source ~/.bash_profile

subject_id=$1
project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
hcp_dir="/gpfs3/well/win-hcp/HCP-YA/subjectsAll"
subject_t1_dir="${hcp_dir}/${subject_id}/T1w"
subject_dti_dir="${project_dir}/data/${subject_id}/drtamas"
out_dir="${project_dir}/data/${subject_id}/drtamas_mm_1mil"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists. Removing contents"
  rm ${out_dir}/*
fi

# Copy template and subject tensors to output directory
imcp ${project_dir}/template/drtamas/dti_nudge_0_1_1_drtamas ${out_dir}/.
imcp ${subject_dti_dir}/dti_tensor_drtamas ${out_dir}/.

# DR-TAMAS subject to template
echo ">>> Running DR-TAMAS between subject and template T1w & DTI images"
DTIREG \
  --fixed_tensor ${out_dir}/dti_nudge_0_1_1_drtamas.nii.gz \
  --moving_tensor ${out_dir}/dti_tensor_drtamas.nii.gz \
  --fixed_scalars ${project_dir}/template/t1.nii.gz \
  --moving_scalars ${subject_t1_dir}/T1w_acpc_dc_restore_brain.nii.gz \
  --metric DTITK \
  --metric Trace \
  --transform SyN[0.3,2,0.1] \
  --convergence 140x140x140x100 \
  --smoothing-sigmas 1.0x1.0x0.5x0.05 \
  --shrink-factors 6x4x2x1 \
  --display_VTKGUI 0 \
  --final_reorientation PPD \
  --optimization_reorientation FS \
  --modality_weights equal \
  --perform_affine 1 \
  --output_combined_disp_field 1

fslmaths ${out_dir}/dti_tensor_drtamas_deffield_MINV ${out_dir}/warp_drtamas_mm_1mil_orig -odt float
fslmaths ${out_dir}/dti_tensor_drtamas_aff_deffield_MINV ${out_dir}/warp_drtamas_mm_1mil_combined_orig -odt float

rm ${out_dir}/dti_nudge_0_1_1_drtamas.nii.gz
rm ${out_dir}/dti_tensor_drtamas.nii.gz
rm ${out_dir}/dti_tensor_drtamas_aff.nii
rm ${out_dir}/dti_tensor_drtamas_diffeo.nii
rm ${out_dir}/dti_tensor_drtamas_deffield_MINV.nii
rm ${out_dir}/dti_tensor_drtamas_aff_deffield_MINV.nii

echo ">>> Subject ${subject_id} completed"