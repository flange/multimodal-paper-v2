#!/bin/bash
# Calculate cluster weights - i.e. the summed t-statistics within significantly activated
# regions, weighted by the grey matter partial volume estimates

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

task_list=(tfMRI_EMOTION tfMRI_GAMBLING tfMRI_LANGUAGE tfMRI_MOTOR tfMRI_RELATIONAL tfMRI_SOCIAL tfMRI_WM)
cope_list=(6 6 6 26 6 6 30)
reg_list=(af fnirt_hcp t1_x4 t1_x2 mm_x2 dti_x2 ants_1mil drtamas_mm_1mil mm_1mil)

for i in ${!task_list[@]}
do
  task_id=${task_list[i]}
  max_cope_id=${cope_list[i]}
  for smooth_id in 0
  do
    submit_file="../submit/job_calculate_cluster_weights_${task_id}_s${smooth_id}.sh"
    if [[ -f ${submit_file} ]];
    then
      rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    for reg_id in ${reg_list[@]}
    do
      echo "./calculate_cluster_weights.sh ${task_id} ${max_cope_id} ${smooth_id} ${reg_id} " >> ${submit_file}
    done
    fsl_sub -t ${submit_file} -l logs/log_calculate_cluster_weights/${task_id}_s${smooth_id}
  done
done