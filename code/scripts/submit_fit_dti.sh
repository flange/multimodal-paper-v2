#!/bin/bash
# Parse list of HCP subjects and create array job for fitting DTI model to each

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi
submit_file="../submit/job_fit_dti.sh"
if [[ -f ${submit_file} ]];
then
  rm ${submit_file}
fi
touch ${submit_file}
chmod u+x $submit_file

# Loop over subjects
subject_id_file="../subjects.txt"
for subject_id in $(cat ${subject_id_file})
do
  # Populate submit script
  echo "./fit_dti.sh ${subject_id}" >> ${submit_file}
done

# Submit task array job
fsl_sub -t ${submit_file} -l log_fit_dti