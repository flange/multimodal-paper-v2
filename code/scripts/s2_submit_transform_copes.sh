#!/bin/bash
# Parse list of HCP subjects and create array job for warping copes to template space

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

task_list=(tfMRI_EMOTION tfMRI_GAMBLING tfMRI_LANGUAGE tfMRI_MOTOR tfMRI_RELATIONAL tfMRI_SOCIAL tfMRI_WM)
cope_list=(6 6 6 26 6 6 30)
reg_id_list=(mm_1mil)

# There are a different number of copes for each task, so this needs seperate loops
subject_id_file="../subjects.txt"

for i in ${!task_list[@]}
do
  task_id=${task_list[i]}
  echo "Creating ${task_id} jobs"
  for reg_id in ${reg_id_list[@]} 
  do
    for smooth_id in 0
    do
      submit_file="../submit/job_transform_copes_${task_id}_s${smooth_id}_${reg_id}.sh"
      if [[ -f ${submit_file} ]];
      then
        rm ${submit_file}
      fi
      touch ${submit_file}
      chmod u+x $submit_file
      for cope_id in $(seq ${cope_list[i]})
      do
        for subject_id in $(cat ${subject_id_file})
        do
          # Populate submit script
          echo "./transform_copes.sh ${subject_id} ${task_id} ${cope_id} ${smooth_id} ${reg_id}" >> ${submit_file}
        done
      done
      # Submit task array job
      fsl_sub -t ${submit_file} -l logs/log_transform_copes/${task_id}_s${smooth_id}_${reg_id}
    done
  done
done