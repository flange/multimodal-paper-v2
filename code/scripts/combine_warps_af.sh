
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Combine warps from MNI to UKB space
source ~/.bash_profile

hcp_warp=$1
ukb_mat=$2
out_warp=$3

# FLIRT subject to template
echo ">>> Running convertwarp"
convertwarp \
  --warp1=${hcp_warp} \
  --postmat=${ukb_mat} \
  --out=${out_warp} \
  --ref=../template/t1 \
  --verbose
echo ">>> Subject completed"