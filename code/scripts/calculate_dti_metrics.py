#!/usr/bin/env python3
#
# Date: 22/08/2022
# Author: Frederik J Lange
# Copyright: FMRIB 2022

import numpy as np
import pandas as pd
from fsl.data.image import Image
import argparse

flags = {
    'dti_a'     : ('-a', '--dti_a'),
    'dti_b'     : ('-b', '--dti_b'),
    'sub_a'     : ('-A', '--sub_a'),
    'sub_b'     : ('-B', '--sub_b'),
    'mask_br'   : ('-mb', '--mask_br'),
    'mask_wm'   : ('-mw', '--mask_wm'),
    'mask_cl'   : ('-ml', '--mask_cl'),
    'mask_cp'   : ('-mp', '--mask_cp'),
    'reg'       : ('-r', '--reg'),
    'out'       : ('-o', '--out')
}
helps = {
    'dti_a'     : 'Prefix for eigendecomposotion files',
    'dti_b'     : 'Prefix for eigendecomposotion files',
    'sub_a'     : 'Name of subject A',
    'sub_b'     : 'Name of subject B',
    'mask_br'   : 'Whole brain mask image',
    'mask_wm'   : 'White matter mask image',
    'mask_cl'   : 'Linear coefficient mask image',
    'mask_cp'   : 'Planar coefficient mask image',
    'reg'       : 'Registration method used to align segmentations',
    'out'       : 'Name of output CSV file'
}

parser = argparse.ArgumentParser(description='Calculate various DTI similarity metrics. Assumes CL and CP volumes exist for ref image')
parser.add_argument(*flags['dti_a'],
                    help=helps['dti_a'],
                    required=True)
parser.add_argument(*flags['dti_b'],
                    help=helps['dti_b'],
                    required=True)
parser.add_argument(*flags['sub_a'],
                    help=helps['sub_a'],
                    required=True)
parser.add_argument(*flags['sub_b'],
                    help=helps['sub_b'],
                    required=True)
parser.add_argument(*flags['mask_br'],
                    help=helps['mask_br'],
                    required=True)
parser.add_argument(*flags['mask_wm'],
                    help=helps['mask_wm'],
                    required=True)
parser.add_argument(*flags['mask_cl'],
                    help=helps['mask_cl'],
                    required=True)
parser.add_argument(*flags['mask_cp'],
                    help=helps['mask_cp'],
                    required=True)
parser.add_argument(*flags['reg'],
                    help=helps['reg'],
                    required=True)
parser.add_argument(*flags['out'],
                    help=helps['out'],
                    required=True)
args = parser.parse_args()

# Read in images
mask_br = Image(args.mask_br)[:]
mask_wm = Image(args.mask_wm)[:]
mask_cl = Image(args.mask_cl)[:]
mask_cp = Image(args.mask_cp)[:]

L1_a = Image(args.dti_a + '_L1')[:]
L2_a = Image(args.dti_a + '_L2')[:]
L3_a = Image(args.dti_a + '_L3')[:]
V1_a = Image(args.dti_a + '_V1')[:]
V2_a = Image(args.dti_a + '_V2')[:]
V3_a = Image(args.dti_a + '_V3')[:]

L1_b = Image(args.dti_b + '_L1')[:]
L2_b = Image(args.dti_b + '_L2')[:]
L3_b = Image(args.dti_b + '_L3')[:]
V1_b = Image(args.dti_b + '_V1')[:]
V2_b = Image(args.dti_b + '_V2')[:]
V3_b = Image(args.dti_b + '_V3')[:]

# Calculate eigenvector dot products
V1_V1 = np.einsum('ijkl,ijkl->ijk', V1_a, V1_b)
V2_V2 = np.einsum('ijkl,ijkl->ijk', V2_a, V2_b)
V3_V3 = np.einsum('ijkl,ijkl->ijk', V3_a, V3_b)

# Calculate OVL
LLVV_sum = np.abs(L1_a*L1_b*V1_V1**2) + np.abs(L2_a*L2_b*V2_V2**2) + np.abs(L3_a*L3_b*V3_V3**2)
LL_sum = np.abs(L1_a*L1_b) + np.abs(L2_a*L2_b) + np.abs(L3_a*L3_b)
OVL_br = np.sum(mask_br[LL_sum > 0] * LLVV_sum[LL_sum > 0] / LL_sum[LL_sum > 0]) / np.count_nonzero(mask_br)
OVL_wm = np.sum(mask_wm[LL_sum > 0] * LLVV_sum[LL_sum > 0] / LL_sum[LL_sum > 0]) / np.count_nonzero(mask_wm)

# Calculate CLV1 and CPV3
CLV1 = np.sum(np.abs(mask_cl[:]*V1_V1)) / np.sum(mask_cl[:])
CPV3 = np.sum(np.abs(mask_cp[:]*V3_V3)) / np.sum(mask_cp[:])

# Create pandas dataframe for storing output
dti_headings = ['reg', 'sub_a', 'sub_b', 'ovl_br', 'ovl_wm', 'clv1', 'cpv3']
dti_data = [[args.reg, args.sub_a, args.sub_b,  OVL_br, OVL_wm, CLV1, CPV3]]
dti_df = pd.DataFrame(data=dti_data, columns=dti_headings)

# Save output to file
dti_df.to_csv(args.out, sep=',', index=False)