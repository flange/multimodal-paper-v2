#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Run T1 only registration using ANTs
# Pass subject ID as input parameter
source ~/.bash_profile

subject_id=$1
project_dir="/gpfs3/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
hcp_dir="/gpfs3/well/win-hcp/HCP-YA/subjectsAll"
subject_t1_dir="${hcp_dir}/${subject_id}/T1w"
subject_dir="${project_dir}/data/${subject_id}"
output_dir="${subject_dir}/ants"

# Create output directory
echo ">>> Creating directory ${output_dir}"
if ! [[ -d ${output_dir} ]];
then
  mkdir -p ${output_dir}
else
  echo "Directory already exists"
fi

# ANTs subject to template
echo ">>> Running ANTs between subject and template T1w images"

${project_dir}/scripts/antsRegistrationSyN_1mil.sh \
  -d 3 \
  -f ${project_dir}/template/t1.nii.gz \
  -m ${subject_t1_dir}/T1w_acpc_dc_restore_brain.nii.gz \
  -o ${output_dir}/ants_1mil_ \
  -p f

echo ">>> Subject ${subject_id} completed"