#!/bin/bash 

get_batch_options() {
    local arguments=("$@")

    unset command_line_specified_study_folder
    unset command_line_specified_subj
    unset command_line_specified_run_local

    local index=0
    local numArgs=${#arguments[@]}
    local argument

    while [ ${index} -lt ${numArgs} ]; do
        argument=${arguments[index]}

        case ${argument} in
            --StudyFolder=*)
                command_line_specified_study_folder=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --Subjlist=*)
                command_line_specified_subj=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --runlocal)
                command_line_specified_run_local="TRUE"
                index=$(( index + 1 ))
                ;;
	    *)
		echo ""
		echo "ERROR: Unrecognized Option: ${argument}"
		echo ""
		exit 1
		;;
        esac
    done
}

get_batch_options "$@"

StudyFolder="/gpfs3/well/win-hcp/HCP-YA/users/sgk882/multimodal_validation/data"
Subjlist="100307 100408 101107 101309 101915 103111 103414 103818 105014 105115 106016 108828 110411 111312 111716 113619 113922 114419 115320 116524 117122 118528 118730 118932 120111 122317 122620 123117 123925 124422 125525 126325 127630 127933 128127 128632 129028 130013 130316 131217 131722 133019 133928 135225 135932 136833 138534 139637 140925 144832 146432 147737 148335 148840 149337 149539 149741 151223 151526 151627 153025 154734 156637 159340 160123 161731 162733 163129 176542 178950 188347 189450 190031 192540 196750 198451 199655 201111 208226 211417 211720 212318 214423 221319 239944 245333 280739 298051 366446 397760 414229 499566 654754 672756 751348 756055 792564 856766 857263 899885" #Space delimited list of subject IDs
EnvironmentScript="/users/win-fmrib-analysis/sgk882/rick_home/multimodal_validation/HCPpipelines-4.3.0-Rick/SetUpHCPPipeline.sh" #Pipeline environment script

if [ -n "${command_line_specified_study_folder}" ]; then
    StudyFolder="${command_line_specified_study_folder}"
fi

if [ -n "${command_line_specified_subj}" ]; then
    Subjlist="${command_line_specified_subj}"
fi

# Requirements for this script
#  installed versions of: FSL, Connectome Workbench (wb_command)
#  environment: HCPPIPEDIR, FSLDIR, CARET7DIR 

#Set up pipeline environment variables and software
source ${EnvironmentScript}

# Log the originating call
echo "$@"

if [ X$SGE_ROOT != X ] ; then
#    QUEUE="-q long.q"
#    QUEUE="-q hcp_priority.q"
		QUEUE="-l log_TaskfMRIAnalysisBatch"
fi

PRINTCOM=""
#PRINTCOM="echo"

########################################## INPUTS ########################################## 

#Scripts called by this script do assume they run on the results of the HCP minimal preprocesing pipelines from Q2

######################################### DO WORK ##########################################

TaskNameList=""
TaskNameList="${TaskNameList} EMOTION"
TaskNameList="${TaskNameList} GAMBLING"
TaskNameList="${TaskNameList} LANGUAGE"
TaskNameList="${TaskNameList} MOTOR"
TaskNameList="${TaskNameList} RELATIONAL"
TaskNameList="${TaskNameList} SOCIAL"
TaskNameList="${TaskNameList} WM"

for TaskName in ${TaskNameList}
do
	LevelOneTasksList="tfMRI_${TaskName}_RL@tfMRI_${TaskName}_LR" #Delimit runs with @ and tasks with space
	LevelOneFSFsList="tfMRI_${TaskName}_RL@tfMRI_${TaskName}_LR" #Delimit runs with @ and tasks with space
	LevelTwoTaskList="tfMRI_${TaskName}" #Space delimited list
	LevelTwoFSFList="tfMRI_${TaskName}" #Space delimited list

	SmoothingList="0" #Space delimited list for setting different final smoothings.  2mm is no more smoothing (above minimal preprocessing pipelines grayordinates smoothing).  Smoothing is added onto minimal preprocessing smoothing to reach desired amount
	LowResMesh="32" #32 if using HCP minimal preprocessing pipeline outputs
	GrayOrdinatesResolution="2" #2mm if using HCP minimal preprocessing pipeline outputs
	OriginalSmoothingFWHM="0" #2mm if using HCP minimal preprocessing pipeline outputes
	Confound="NONE" #File located in ${SubjectID}/MNINonLinear/Results/${fMRIName} or NONE
	#Confound="Movement_Regressors_dt.txt" #File located in ${SubjectID}/MNINonLinear/Results/${fMRIName} or NONE
	TemporalFilter="200" #Use 2000 for linear detrend, 200 is default for HCP task fMRI
	VolumeBasedProcessing="YES" #YES or NO. CAUTION: Only use YES if you want unconstrained volumetric blurring of your data, otherwise set to NO for faster, less biased, and more senstive processing (grayordinates results do not use unconstrained volumetric blurring and are always produced).  
	RegNames="NONE" # Use NONE to use the default surface registration
	ParcellationList="NONE" # Use NONE to perform dense analysis, non-greyordinates parcellations are not supported because they are not valid for cerebral cortex.  Parcellation superseeds smoothing (i.e. smoothing is done)
	ParcellationFileList="NONE" # Absolute path the parcellation dlabel file


	for RegName in ${RegNames} ; do
		j=1
		for Parcellation in ${ParcellationList} ; do
			ParcellationFile=`echo "${ParcellationFileList}" | cut -d " " -f ${j}`

			for FinalSmoothingFWHM in $SmoothingList ; do
				echo $FinalSmoothingFWHM
				i=1
				for LevelTwoTask in $LevelTwoTaskList ; do
					echo "  ${LevelTwoTask}"

					LevelOneTasks=`echo $LevelOneTasksList | cut -d " " -f $i`
					LevelOneFSFs=`echo $LevelOneFSFsList | cut -d " " -f $i`
					LevelTwoTask=`echo $LevelTwoTaskList | cut -d " " -f $i`
					LevelTwoFSF=`echo $LevelTwoFSFList | cut -d " " -f $i`

					for Subject in $Subjlist ; do
						echo "    ${Subject}"
						
						if [ -n "${command_line_specified_run_local}" ] ; then
						    echo "About to run ${HCPPIPEDIR}/TaskfMRIAnalysis/TaskfMRIAnalysis.sh"
						    queuing_command=""
						else
						    echo "About to use fsl_sub to queue or run ${HCPPIPEDIR}/TaskfMRIAnalysis/TaskfMRIAnalysis.sh"
						    queuing_command="${FSLDIR}/bin/fsl_sub ${QUEUE}"
						fi

						${queuing_command} ${HCPPIPEDIR}/TaskfMRIAnalysis/TaskfMRIAnalysis.sh \
						    --path=$StudyFolder \
						    --subject=$Subject \
						    --lvl1tasks=$LevelOneTasks \
						    --lvl1fsfs=$LevelOneFSFs \
						    --lvl2task=$LevelTwoTask \
						    --lvl2fsf=$LevelTwoFSF \
						    --lowresmesh=$LowResMesh \
						    --grayordinatesres=$GrayOrdinatesResolution \
						    --origsmoothingFWHM=$OriginalSmoothingFWHM \
						    --confound=$Confound \
						    --finalsmoothingFWHM=$FinalSmoothingFWHM \
						    --temporalfilter=$TemporalFilter \
						    --vba=$VolumeBasedProcessing \
						    --regname=$RegName \
						    --parcellation=$Parcellation \
						    --parcellationfile=$ParcellationFile
						
					done

					i=$(($i+1))

				done
				
			done

			j=$(( ${j}+1 ))

		done

	done

done
